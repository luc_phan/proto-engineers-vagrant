class common::config inherits common {

  if $operatingsystem == 'CentOS' and $operatingsystemmajrelease == 7 {

    file { '/etc/bashrc':
      source => 'puppet:///modules/common/centos7/bashrc',
      owner => root,
      group => root,
      mode => 644
    }

    file { '/root/.vimrc':
      source => 'puppet:///modules/common/centos7/vimrc',
      owner => root,
      group => root,
      mode => 644
    }

    file { '/home/vagrant/.vimrc':
      source => 'puppet:///modules/common/centos7/vimrc',
      owner => vagrant,
      group =>  vagrant,
      mode => 644
    }

    file { '/home/vagrant/.vim':
      source  => 'puppet:///modules/common/centos7/_vim',
      owner   => vagrant,
      group   =>  vagrant,
      mode    => 644,
      recurse => true
    }

  }

}
