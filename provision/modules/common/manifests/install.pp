class common::install inherits common {
  package { 'vim-enhanced': ensure => 'installed' }
}
